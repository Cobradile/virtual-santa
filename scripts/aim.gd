extends Camera

var power = 1
var score = 0 setget setScore
var enemyCount = 0
var currentPart = 1
var canShoot = true
var result
var direction = Vector3()
var health = 5

onready var muzzle = $Muzzle

onready var cookie = preload("res://resources/Cookie.tscn")
var cookieSfx = preload("res://sfx/cookie_shot.wav")

# Called when the node enters the scene tree for the first time.
func _ready():
	self.score = 0
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func _input(event):
	if event is InputEventMouseMotion:
		ADSAim(event.position, 0)
	if event.is_action_pressed("Shoot"):
		shoot(result)

func _process(delta):
	$Crosshair.position = get_viewport().get_mouse_position()

func ADSAim(position, randomSpreid):
	var from = project_ray_origin(position)
	var to = from + project_ray_normal(position) * 1000
	to.x += (randf() - 0.5) * randomSpreid
	to.y += (randf() - 0.5) * randomSpreid
	to.z += (randf() - 0.5) * randomSpreid
	var space_state = get_world().direct_space_state
	result = get_world().direct_space_state.intersect_ray(from, to, [self])

func shoot(target):
	if canShoot:
		if target != null:
			if !target.empty() and !(target is Vector3):
				_fireCookie(target)
				if target.collider.is_in_group("House"):
					if !target.collider.delivered:
						$Particles.look_at(target.collider.global_transform.origin, Vector3.UP)
						$Particles.restart()
						self.score += 10
						$Label.text = "Score: " + String(score)
						$Particles.emitting = true
						canShoot = false
						$Timer.start()
				elif target.collider.is_in_group("AI"):
					enemyCount += 1
					#print(str(enemyCount) + " out of " + str(get_node("../Room " + str(currentPart)).enemies.size()))
					self.score += 10
					# something is wrong with the code below here
					# invalid get index 'enemies'
					if currentPart <= 3:
						if str(currentPart) and enemyCount >= get_node("../Room " + str(currentPart)).enemies.size():
							currentPart += 1
							enemyCount = 0
							$"../AnimationPlayer".play("Part " + str(currentPart))
					else:
						get_tree().quit()

func _fireCookie(target):
	var c = cookie.instance()
	muzzle.add_child(c)
	c.look_at(target.position, Vector3.UP)
	c.shoot = true
	$AudioStreamPlayer3D.stream = cookieSfx
	$AudioStreamPlayer3D.play()

func setScore(newScore):
	score = newScore
	$Label.text = "Score: " + str(score)

func takeDamage(damage):
	health -= damage
	if health <= 0:
		die()

func die():
	# do die stuff
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	get_tree().change_scene("res://resources/GameOVer.tscn")

func _on_Timer_timeout():
	canShoot = true

func _on_AnimationPlayer_animation_finished(anim_name):
	get_node("../Room " + str(currentPart)).activate()
