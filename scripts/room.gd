extends Node

var enemies = Array()

# Called when the node enters the scene tree for the first time.
func _ready():
	yield(get_tree().create_timer(1), "timeout")
	for collider in get_children():
		if collider.is_in_group("AI"):
			enemies.append(collider)
	deactivate()

func activate():
	for collider in enemies:
		collider.get_node("CollisionShape").disabled = false
		collider.enable()

func deactivate():
	for collider in enemies:
		collider.get_node("CollisionShape").disabled = true
		collider.disable()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
