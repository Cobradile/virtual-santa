# TODO: Add santa behaviour
extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var target = null;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _target_locked(new_target: Spatial) -> void:
	target = new_target
	
func _target_released() -> void:
	target = null
