extends Spatial

signal timeout
export(int, 100) var health = -1
var shoot_time = 2 # 500ms
var time = 0
var enabled = false
onready var aim = load("res://scripts/aim.gd").new()


# Called when the node enters the scene tree for the first time.
func _ready():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	shoot_time = rng.randi() % 5 + 5
	print("set shoottime %s", shoot_time)

func _process(delta):
	time += delta
	if time > shoot_time and enabled:
		emit_signal("timeout")
		var rng = RandomNumberGenerator.new()
		rng.randomize()
		shoot_time = rng.randi() % 5 + 5
		_shoot()
		time = 0

func _shoot():
	#do shooty stuff
	hitPlayer()

func hitPlayer():
	get_node("/root/Spatial/Camera").takeDamage(1)

func die():
	$CollisionShape.disabled = true
	$RootNode/AnimationPlayer.play("die")

func enable(): 
	enabled = true

func disable():
	enabled = false

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name.match("die"):
		queue_free()
